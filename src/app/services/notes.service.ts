import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { constants } from '../config/constants';
import { Note } from '../notes/note.interface';
import { BehaviorSubject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  // Local buffer (Reason: Perfomance)
  public notes$: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>(null);

  constructor(
    private http: HttpClient,
    private snackbar: MatSnackBar
  ) { }

  /**
   * Retrieve notes from the API
   *  Append notes to local buffer
   *  Display snackbar depending on result
   */
  getNotes() {
    return this.http.get<Note[]>(`${constants.api}/notes`).pipe(
      map(res => {
        this.notes$.next(res);
        this.snackbar.open('Succesfully retrieved notes!', 'OK', { duration: 1000 });
        return res;
      }),
      catchError(err => {
        this.snackbar.open('Something went wrong getting the notes! (API Offline?)', 'OK', { duration: undefined });
        return err;
      })
    );
  }

  /**
   * Creates note through the API 
   * @param note the note containing: title and text
   *  Appends note returned from POST call API to local buffer (Reason: Performance, removes the need to re-download notes list)
   *  Display snackbar depending on result
   */
  createNote(note: Note) {
    return this.http.post<Note>(`${constants.api}/notes`, note).pipe(
      map(res => {
        this.notes$.next(this.notes$.value.concat(res));
        this.snackbar.open('Succesfully created!', 'OK', { duration: 1000 });
        return res;
      }),
      catchError(err => {
        this.snackbar.open('Something went wrong creating the note!', 'OK', { duration: 1000 });
        return err;
      })
    );
  }

  /**
   * Deletes a note through the API
   * @param id id of the note to delete
   *  Removes note upon API success from local buffer (Reason: Performance, removes the need to re-download notes list)
   *  Display snackbar depending on result
   */
  deleteNote(id: string) {
    return this.http.delete(`${constants.api}/notes/${id}`).pipe(
      map(res => {
        this.notes$.next(this.notes$.value.filter(note => note.id !== id));
        this.snackbar.open('Succesfully deleted!', 'OK', { duration: 1000 });
        return true;
      }),
      catchError(err => {
        this.snackbar.open('Something went wrong deleting the note!', 'OK', { duration: 1000 });
        return err;
      })
    );
  }

  /**
   * Saves a note through the API
   * @param note the note containing either title or text
   *  Display snackbar depending on result
   */
  saveNote(note: Note) {
    return this.http.put(`${constants.api}/notes/${note.id}`, note).pipe(
      map(res => {
        this.snackbar.open('Succesfully saved!', 'OK', { duration: 1000 });
        return true;
      }),
      catchError(err => {
        this.snackbar.open('Something went wrong saving the note!', 'OK', { duration: 1000 });
        return err;
      })
    );
  }
}
