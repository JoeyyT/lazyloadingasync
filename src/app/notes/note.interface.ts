export interface Note {
    title: string;
    text: string;
    id?: string;
    deleting?: boolean; // client only
    editing?: boolean; // client only
    saving?: boolean; // client only
}