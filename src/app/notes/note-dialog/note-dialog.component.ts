import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Note } from '../note.interface';
import { MdEditorOption } from 'ngx-markdown-editor';

@Component({
  selector: 'app-note-dialog',
  templateUrl: './note-dialog.component.html',
  styleUrls: ['./note-dialog.component.scss']
})
export class NoteDialogComponent implements OnInit {

  note: Note;
  public mode: string = "editor";

  public options: MdEditorOption = {
    showPreviewPanel: true,
    enablePreviewContentClick: false,
    resizable: false,
    customRender: {
      // image: function (href: string, title: string, text: string) {
      //   let out = `<img style="max-width: 100%; border: 20px solid red;" src="${href}" alt="${text}"`;
      //   if (title) {
      //     out += ` title="${title}"`;
      //   }
      //   out += (<any>this.options).xhtml ? "/>" : ">";
      //   return out;
      // }
    }
  };

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.note = data.note;
  }

  ngOnInit(): void {
  }

}
