import { Component, OnInit } from '@angular/core';
import { Note } from './note.interface';
import { NotesService } from '../services/notes.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  // Local buffer from service
  notes$: Observable<Note[]> = this.notesService.notes$;

  constructor(
    private notesService: NotesService
  ) { }

  ngOnInit(): void {
    this.notesService.getNotes().subscribe();
  }

  /**
   * Creates a note
   */
  createNote(): void {
    const note: Note = {
      title: "",
      text: ""
    }
    this.notesService.createNote(note).subscribe();
  }

  /**
   * Deletes a note
   * @param note the note containing: title and text
   *  Initiates and stops a loading stage: 'deleting' depending on API request
   *  (TEMP) Removes deleting field from note
   */
  deleteNote(note: Note) {
    this.notesService.deleteNote(note.id).subscribe();
  }
}
