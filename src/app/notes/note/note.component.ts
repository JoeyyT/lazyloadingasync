import { Component, OnInit, Input } from '@angular/core';
import { NotesService } from 'src/app/services/notes.service';
import { Note } from '../note.interface';
import { NoteDialogComponent } from '../note-dialog/note-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MdEditorOption } from 'ngx-markdown-editor';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
})
export class NoteComponent implements OnInit {

  @Input() note: Note;

  saving: boolean;
  deleting: boolean;

  constructor(private notesService: NotesService, private dialog: MatDialog) { }

  public options: MdEditorOption = {
    showPreviewPanel: false,
    enablePreviewContentClick: false,
    resizable: false,
    customRender: {
      // image: function (href: string, title: string, text: string) {
      //   let out = `<img style="max-width: 100%; border: 20px solid red;" src="${href}" alt="${text}"`;
      //   if (title) {
      //     out += ` title="${title}"`;
      //   }
      //   out += (<any>this.options).xhtml ? "/>" : ">";
      //   return out;
      // }
    }
  };

  ngOnInit(): void {
  }

  /**
  * Saves a note
  * @param note the note containing: title and text
  *  Initiates and stops a loading stage: 'saving' depending on API request
  */
  save(note: Note) {
    this.saving = true;
    this.notesService.saveNote(note).subscribe(res => {
      this.saving = false;
    });
  }

  /**
  * Removes a note
  * @param note the note containing: title and text
  *  Initiates and stops a loading stage: 'deleting' depending on API request
  */
  remove(note: Note) {
    this.deleting = true;

    this.notesService.deleteNote(note.id).subscribe(res => {
      this.deleting = false;
    })
  }

  edit(note: Note) {
    const dialogRef = this.dialog.open(NoteDialogComponent, {
      data: {
        note
      },
      width: '100%',
      height: '95%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.save(result)

      }
    });
  }

}
